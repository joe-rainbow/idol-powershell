
$cfs_service = "HPE-IDOLServer-ConnectorFrameworkServer"
$bifhi_service = "HPE-IDOLServer-BiFHI"
$web_service = "HPE-IDOLServer-WebConnector"
$file_service = "HPE-IDOLServer-FileSystemConnector"
$site_admin_service = "HPE-IDOLServer-SiteAdmin"
$idol_service = "HPE-IDOLServer"
$controller_service = "HPE-IDOLServer-Controller"
$coordinator_service = "HPE-IDOLServer-Coordinator"
$eduction_service = "HPE-IDOLServer-EductionEngine"
$image_service = "HPE-IDOLServer-ImageServer"


$web_connector_folder = "C:\HewlettPackardEnterprise\Connectors\Web"
$file_connector_folder = "C:\HewlettPackardEnterprise\Connectors\FileSystem"
<#
Restarts all IDOL Services
#>

function IDOL-Bounce-All {
	$services = Get-Service | Where-Object {$_.Name -match 'HPE'}
	foreach ($service in $services){
		Restart-Service $service.Name -Force -Verbose
	}

	Get-Service | Where-Object {$_.Name -match 'HPE'} | select DisplayName, Status | Format-List
}
<#
Restarts the connector framework server
#>
function IDOL-Bounce-CFS {
	Restart-IDOLService($cfs_service)
}
<#
Starts the connector framework server
#>
function IDOL-Start-CFS {
	$svc = Get-Service $cfs_service
	if ($svc.Status -eq "Stopped"){
		Start-Service $cfs_service -Verbose
	}
	else{
		Write-Host $cfs_service "is currently running."
	}
	IDOL-Get-Services-Status	
}
<#
Stop the connector framework server
#>
function IDOL-Stop-CFS {
	$svc = Get-Service $cfs_service
	if ($svc.Status -eq "Running"){
		Stop-Service $cfs_service -Verbose
	}
	else{
		Write-Host $cfs_service "is currently not running."
	}
	IDOL-Get-Services-Status
}
<#
Starts the Web Connector
#>
function IDOL-Start-Web {
	$svc = Get-Service $web_service
	if ($svc.Status -eq "Stopped"){
		Start-Service $svc -Verbose
	}
	else{
		Write-Host $web_service "is currently running."
	}
	IDOL-Get-Services-Status	
}
<#
Stop the Web Connector
#>
function IDOL-Stop-Web {
	$svc = Get-Service $web_service
	if ($svc.Status -eq "Running"){
		Stop-Service $web_service -Verbose
	}
	else{
		Write-Host $web_service "is currently not running."
	}
	IDOL-Get-Services-Status
}
<#
Delete all databases in Web Connector
#>
function IDOL-Del-DB-Web{
	$svc_cfs = Get-Service $cfs_service
	Write-Host "Stopping " $svc_cfs.DisplayName
	if ($svc_cfs.Status -eq "Running"){
		Stop-Service $cfs_service
	}
	else{
		Write-Host $cfs_service "is currently not running."
	}
	Get-Service $cfs_service
	$svc = Get-Service $web_service
	if ($svc.Status -eq "Stoppped"){
		Write-Host "Web connector stopped"
		Get-ChildItem -Path $web_connector_folder -Include *.db -File -Recurse | foreach {
			Write-Host "Deleting file " $_.Name
			$_.Delete()
		}
	}
	else{
		Write-Host "Stopping " $svc.DisplayName
		Stop-Service $web_service
		Get-Service $web_service
		Get-ChildItem -Path $web_connector_folder -Include *.db -File -Recurse | foreach {
			Write-Host "Deleting file " $_.Name
			$_.Delete()
		}
	}
	Write-Host "Starting the CFS"
	Start-Service $cfs_service
	Get-Service $cfs_service
	Write-Host "Starting the web connector"
	Start-Service $web_service
	Get-Service $web_service

}

<#
Delete all database in the File System Connector 
#>
function IDOL-Del-DB-File{
	$svc_cfs = Get-Service $cfs_service
	Write-Host "Stopping " $svc_cfs.DisplayName
	if ($svc_cfs.Status -eq "Running"){
		Stop-Service $cfs_service
	}
	else{
		Write-Host $cfs_service "is currently not running."
	}
	Get-Service $cfs_service
	$svc = Get-Service $file_service
	if ($svc.Status -eq "Stoppped"){
		Write-Host "File System connector stopped"
		Get-ChildItem -Path $file_connector_folder -Include *.db -File -Recurse | foreach {
			Write-Host "Deleting file " $_.Name
			$_.Delete()
		}
	}
	else{
		Write-Host "Stopping " $svc.DisplayName
		Stop-Service $svc
		Get-ChildItem -Path $file_connector_folder -Include *.db -File -Recurse | foreach {
			Write-Host "Deleting file " $_.Name
			$_.Delete()
		}
	}
	Write-Host "Starting the CFS"
	Start-Service $cfs_service
	Get-Service $cfs_service
	Write-Host "Starting the file system connector"
	Start-Service $file_service
	Get-Service $file_service

}

<#
Restarts the BiFHI
#>
function IDOL-Bounce-BiFHI {
	Restart-IDOLService($bifhi_service)
}
<#
Restarts the Eduction Server
#>

function IDOL-Bounce-Eduction {
	Restart-IDOLService($eduction_service)
}
<#
Restart the Web connector
#>
function IDOL-Bounce-Conn-Web {
	Restart-IDOLService($web_service)
}
<#
Restart the File System connector
#>
function IDOL-Bounce-Conn-File {
	Restart-IDOLService($file_service)
}
<#
Start the File System Connector
#>
function IDOL-Start-File{
	$svc = Get-Service $file_service
	if ($svc.Status -eq "Stopped"){
		Start-Service $file_service -verbose
	}
	else{
		Write-Host $file_service "is currently not running."
	}
	IDOL-Get-Services-Status
}
<#
Stop the File System Connector
#>
function IDOL-Stop-File{
	$svc = Get-Service $file_service
	if ($svc.Status -eq "Running"){
		Stop-Service $file_service -verbose
	}
	else{
		Write-Host $file_service "is currently running."
	}
	IDOL-Get-Services-Status
}
<#
Restart the Image Server
#>
function IDOL-Bounce-Conn-File {
	$service_name = "HPE-IDOLServer-FileSystemConnector"
	Restart-IDOLService($service_name)
}
<#
Private global function for restarting a given service
#>
function global:Restart-IDOLService($service_name){
	$services = Get-Service | Where-Object {$_.Name -match $service_name}
	foreach ($service in $services){
		Restart-Service $service.Name -Force -Verbose
	}

	Get-Service | Where-Object {$_.Name -match $service_name} | select DisplayName, Status | Format-List
}
<#
Find all IDOL Services which are not running and start them
#>
function IDOL-Start-AllStopped-Services{
	$stopped_IDOL_Services = Get-Service | Where-Object {$_.Name -match "HPE"} | Where-Object {$_.Status -eq "Stopped"}
	foreach ($service in $stopped_IDOL_Services){
		Start-Service $service.Name -Verbose
		Get-Service | Where-Object {$_.Name -match $service} | select DisplayName, Status | Format-List
	}
}
<#
Show me all IDOL services which are not running
#>
function global:IDOL-Show-Stopped-Services {
	Get-Service | Where-Object {$_.Name -match "HPE"} | Where-Object {$_.Status -eq "Stopped"} | select DisplayName, Status | Format-List
}
<#
Show me the status of all IDOL Services
#>
function global:IDOL-Get-Services-Status {
	Get-Service | Where-Object {$_.Name -match "HPE"} | select DisplayName, Status | Format-Table
}
<#
Show all the functions in this module
#>
function IDOL-Get-PS-Functions{
	 Get-Module IDOLModules | foreach {�`r`nmodule name: $_�; �`r`n�;gcm -Module $_.name -CommandType cmdlet, function | select name}
}
<#
Print IDOL services status
#>
function IDOL-ServiceStatus{
	IDOL-Get-Services-Status
}
<#
Print IDOL services which are stopped
#>
function IDOL-StoppedServices{
	IDOL-Show-Stopped-Services
}
<#
Stop CFS and connectors
#>
function IDOL-Stop-Ingestion{
	$result = Invoke-RestMethod -Uri "http://localhost:9001/DRERESET" 
	$threads = $result.split('=')[1]
	Write-Host "IDOL currently has $($threads) threads"
	$url = "http://localhost:9000/action=IndexerGetStatus&IndexAction=Cancel&Index=1-$($threads)&responseformat=json"
	$cancel_result = Invoke-RestMethod -Uri $url | `
	Select-Object -ExpandProperty autnresponse | `
	Select-Object -ExpandProperty responsedata | `
	Select-Object -ExpandProperty item | foreach {
		Write-Host $_.id " " $_.status
	}
	Write-Host $cancel_result
	$svcs = Get-Service | Where-Object {$_.Name -match "connector"} | foreach {
		Write-Host "Stopping : " $_.DisplayName
		Stop-Service $_.Name
		Write-Host "Status of " $_.DisplayName " = " $_.Status
	}
}
<#
Flush Actions folder in CFS and connectors
#>
function IDOL-Flush-Actions{
	Get-ChildItem 
}